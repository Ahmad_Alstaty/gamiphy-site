<?php
/**
 * Template Name: GAM pricing  
 *
 */

get_header('inner'); ?>

	<!--=================== content area ================================== -->
	<div class="container">
		<?php if( have_rows('header_content') ): ?>
        <?php while( have_rows('header_content') ): the_row(); 

          $heading  = get_sub_field('heading');
          $sub_heading  = get_sub_field('sub_heading');
          $left_image = get_sub_field('left_image');

          ?>

		<div class="col-md-12 sub-header pricing" style=" background-image: url('<?php echo $left_image['url']; ?>'); ">
			
		
			<h2><?php echo $heading; ?></h2>
			<p><?php echo $sub_heading; ?></p>
			

			<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<!-- plans -->
		<?php if( have_rows('pricing_plan') ): ?>
           
		<div class="col-md-12 plansgami">
			 <?php $i = 0 ; while( have_rows('pricing_plan') ): the_row(); 

              // vars
              $heading = get_sub_field('heading');
              $excerp  = get_sub_field('excerp');
              $plan  = get_sub_field('plan');
              $user_range  = get_sub_field('user_range');
              $link  = get_sub_field('link');
              $link_label = get_sub_field('link_label');
              $package_points  = get_sub_field('package_points');
              

              ?>

			<div class="col-md-4">
		

				<div class="plans-gami-inner  <?php if ($i==1) {?> premium <?php } ?>" >
					<h3 class="head-plan"><?php echo $heading; ?></h3>
					
					<p  class="descrip-plan"><?php echo $excerp; ?></p>

					
					<h4 class="free">
						
						<span class="price "><?php echo $plan; ?> </span> 

						<?php if ($i==1) {?> 
							
							<!-- <span id="estimate-label">$<input type="text" id="dollar" value="" ></span><span>/month</span>
							 -->

						<?php } ?>
					</h4>
					


					<p class="user-ra"><?php echo $user_range; ?></p>


					<?php if ($i==1) {?> 
						 <!-- <p  class="user-ra"><span id="user-label">10,000</span> <span>Members</span></p>  -->
					<?php } ?>
					
					
					<div class="plan-range">
					<?php if ($i==1) {?>
						<!-- <label class="left-amount"><span>50,000</span><br/>Members</label>
						<label class="right-amount">
						<input type="hidden" id="amount" value="5000" >
						<span>5000</span>
						<br/>Members
						</label>
						<div id="slider-range"></div> -->
						
						<?php } ?>
					</div>



					

					<a href="<?php echo site_url(); ?>/<?php echo $link; ?>" class="btn plansbtn"><?php echo $link_label; ?></a>
					<?php echo $package_points; ?>

				</div>
			</div>

			<?php $i++; endwhile; ?>

		</div>

		<?php endif; ?>

		<!-- faqs -->
		<div class="col-md-12 faqhere">
			
			<?php  $faq_heading = get_field('faq_heading' );

			echo $faq_heading;
			 ?>
			<?php

			global $post;
			$args = array( 'posts_per_page' => -1,  'post_type' => 'faqs','offset'=> 1,'order'=> 'ASC' );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post();
			?>
			<article class="faq col-md-7">
				<?php the_title( sprintf( '<h5><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h5>' ); ?>
				<?php the_content(); ?>
			</article>
			
			<?php  endwhile; 
			wp_reset_postdata();?>

			<div class="text-center faq-link">
		
			<?php  $no_question = get_field('no_question' );

			echo $no_question;
			 ?>

			</div>
		</div>

	</div>
<?php if( have_rows('unlock_our_premium_plan') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('unlock_our_premium_plan') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content  = get_sub_field('content');
              $label  = get_sub_field('label');
              $link  = get_sub_field('link');

              ?>

					<div class="col-md-8">
						<h2><?php echo $content; ?></h2>
						<div class="btn-wrap">
						<?php if( $link ): ?>
							<a href="<?php echo $link; ?>" class="btn button-2"><?php echo $label; ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-3 ">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					</div>
				<?php endwhile; ?>

          		
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if( have_rows('customers_journey') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('customers_journey') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $label  = get_sub_field('label');

              ?>

				<div class="col-md-3">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8">
					<h2><?php echo $title; ?></h2>
					<div class="btn-wrap">
					<?php if( $link1 ): ?>
					<a href="<?php echo $link1; ?>" class="btn button-2"><?php echo $label; ?></a>
					<?php endif; ?>
				</div>
				</div>
				<?php endwhile; ?>

			</div>
		</div>
	</section>
	<?php endif; ?>



	<!--=================== content area end ================================== -->

<?php get_footer(); ?>


	<script type="text/javascript">
	
	$(document).ready(function(){
		var getValue = $('#amount').val();
		//alert(getValue);

		
	});
	

	</script>