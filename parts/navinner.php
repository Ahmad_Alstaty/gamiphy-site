<nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo site_url(); ?>">
            <?php 
                          $custom_logo_id = get_theme_mod( 'custom_logo' );
              $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
              if ( has_custom_logo() ) {
                      echo '<img src="'. esc_url( $logo[0] ) .'"  width="116px">';
              } else {
                      echo ' <img src="/wp-content/uploads/2018/12/logo_gamiphy-1.png" width="116px">';
              }
            ?>
           
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
                <?php
                  wp_nav_menu( array(
                    'menu' => 'Top Menu',
                    'theme_location' => 'none',
                    'menu_class'     => 'nav navbar-nav',
                    'container' => 'ul',
                   ) );
                ?>
          
          </div><!--/.nav-collapse -->

         <div class="user">
           <a href="<?php echo site_url(); ?>/demo" class="userbtn">Request Demo <img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
            <a href="https://app.gamiphy.co/index.html" title="Members Area Login" rel="home">
            <span class="userpic"><img src="<?php echo get_template_directory_uri(); ?>/images/ic-account-inner.svg');" alt=""></span>
            </a>
            <?php //if ( is_user_logged_in() ) { ?>
                <!-- <a href="<?php echo site_url(); ?>"> <span class="userpic"><?php echo get_avatar( $author_id ) ; ?></span></a> -->
            <?php //} else { ?>
                <!-- <a href="<?php echo site_url(); ?>/login" title="Members Area Login" rel="home"> 
                <span class="userpic"><img src="<?php echo get_template_directory_uri(); ?>/images/ic-account-inner.svg');" alt=""></span>
                </a> -->
            <?php //} ?>
           

          
          </div>


        </nav>