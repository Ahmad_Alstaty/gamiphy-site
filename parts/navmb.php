<nav class="navbar navbar-default mobmenu">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbarmb" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <img src="<?php echo get_template_directory_uri(); ?>/images/hamburger_open.png" width="30px"  alt="" class="img-swap">
      </button>
       <a class="navbar-brand" href="<?php echo site_url(); ?>">
            <?php 
                          $custom_logo_id = get_theme_mod( 'custom_logo' );
              $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
              if ( has_custom_logo() ) {
                      echo '<img src="'. esc_url( $logo[0] ) .'"  width="116px">';
              } else {
                      echo ' <img src="/wp-content/uploads/2018/12/logo_gamiphy-1.png" width="116px">';
              }
            ?>
           
            </a>
    </div>
    <div id="navbarmb" class="navbar-collapse collapse">
         <ul id="menu-top-menu" class="nav navbar-nav">
         <li class="">
         <a href="<?php echo site_url(); ?>/features">
         <img src="<?php echo get_template_directory_uri(); ?>/images/m-gamibot.png" alt="">Features</a>
         </li>
          <li class="">
          <a href="<?php echo site_url(); ?>/pricing">
          <img src="<?php echo get_template_directory_uri(); ?>/images/m-pricing.png" alt="">
          Pricing</a>
          </li>
          <!--<li class="">-->
          <!--<a href="<?php echo site_url(); ?>/about-us">-->
          <!--<img src="<?php echo get_template_directory_uri(); ?>/images/m-about.png" alt="">-->
          <!--About us</a>-->
          <!--</li>-->
          <!--<li class="">-->
          <!-- <a href="<?php echo site_url(); ?>/blog">-->
          <!-- <img src="<?php echo get_template_directory_uri(); ?>/images/m-blog.png" alt="">-->
          <!--  Blog</a>-->
          <!--</li>-->
        </ul>

          <div class="usermb">
           <a href="<?php echo site_url(); ?>/demo" class="btn mbbtn">Request Demo<img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
            <a href="https://app.gamiphy.co/index.html" title="Members Area Login" rel="home"  class="acount">
            <span class="userpic"><img src="<?php echo get_template_directory_uri(); ?>/images/ic-account-inner.svg');" alt=""></span>
            </a>
            <?php //if ( is_user_logged_in() ) { ?>
                <!-- <a href="<?php echo site_url(); ?>"> <span class="userpic"><?php echo get_avatar( $author_id ) ; ?></span></a> -->
            <?php //} else { ?>
                <!-- <a href="<?php echo site_url(); ?>/login" title="Members Area Login" rel="home"> 
                <span class="userpic"><img src="<?php echo get_template_directory_uri(); ?>/images/ic-account-inner.svg');" alt=""></span>
                </a> -->
            <?php //} ?>
           
          </div>
    
    </div><!--/.nav-collapse -->
  
    </nav>

    
  </nav>
