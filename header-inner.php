<?php get_template_part( 'parts/head'); ?>


<header class="header-custom-inner">
	<div class="header-wrap">
	<div class="container">

		  <?php get_template_part( 'parts/navinner'); ?>
		   <?php get_template_part( 'parts/navmb'); ?>

    </div>

</header>
<div class="clearfix"></div>

<body <?php body_class(); ?>>
