<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 */
?>

	<footer class="footer-custom">
		<div class="container">
			<div class="col-md-3 col-xs-6">
				<h5 class="h5">GAMIPHY</h5>
				
				<?php
                  wp_nav_menu( array(
                    'menu' => 'Footer 1',
                    'theme_location' => 'none',
                    'menu_class'     => 'no-class',
                    'container' => 'ul',
                   ) );
                ?>
			</div>
			<div class="col-md-3 col-xs-6">
				<h5 class="h5">SUPPORT</h5>
				<ul class="no-class">
					<li>
					<a class="openchat" href="javascript:void(0)">Contact Support</a>
					</li>
				</ul>

				<?php
                  // wp_nav_menu( array(
                  //   'menu' => 'Footer 2',
                  //   'theme_location' => 'none',
                  //   'menu_class'     => 'no-class',
                  //   'container' => 'ul',
                  //  ) );
                ?>

			</div>
			<div class="col-md-3  col-xs-12">
				<h5 class="h5">COMPANY</h5>
				
				<?php
                  // wp_nav_menu( array(
                  //   'menu' => 'Footer 3',
                  //   'theme_location' => 'none',
                  //   'menu_class'     => 'no-class',
                  //   'container' => 'ul',
                  //  ) );
                ?>
                <ul class="no-class">
					<li>
					<a class="openchat" href="javascript:void(0)">Contact us</a>
					</li>
					<li>
						<a href="/privacy-policy/">Privacy Policy</a>
					</li>
				</ul>

			</div>
			
			<div class="col-md-3  col-xs-12 subs">
				<h5 class="h5">SUBSCRIBE TO OUR NEWSLETTER</h5>
				<?php echo do_shortcode('[mailpoet_form id="1"]'); ?>
			</div>

			<div class="col-md-6  col-xs-12 social">
				
				<?php if ( function_exists('cn_social_icon') ) echo cn_social_icon(); ?>
			</div>
			<div class="col-md-6 col-xs-12  copyrigh">
				<?php get_sidebar( 'content-bottom' ); ?>
			</div>
		</div>
		<div class="rectangle-right">
		
		</div>
	</footer>


	<!--=================== scrips  ================================== -->
  

<?php wp_footer(); ?>

 	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.3.1.js"></script>
   	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
   	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.js"></script>
   	<script src="<?php echo get_template_directory_uri(); ?>/js/custom-function.js"></script>
   	
   	<script type="text/javascript">
   	$(document).ready(function(){$('#openchat').click(function(){Intercom('show');});
	   		$("#openchat").attr( 'href','javascript:void(0)');
    });
    $(document).ready(function(){$('.openchat').click(function(){Intercom('show');});
	   		$(".openchat").attr( 'href','javascript:void(0)');
    });

   	</script>
<!--Weglot 2.5.0--><aside data-wg-notranslate class='country-selector weglot-dropdown weglot-default'><input id="wg15512976215c76ec558ea0c978" class="weglot_choice" type="checkbox" name="menu"/><label for="wg15512976215c76ec558ea0c978" class="wgcurrent wg-li weglot-flags en" data-code-language="en"><span>EN</span></label><ul><li class="wg-li weglot-flags es" data-code-language="es"><a data-wg-notranslate href="https://gamiphy.co/es/wp-admin/admin-ajax.php">ES</a></li></ul></aside> </body>
</html>
