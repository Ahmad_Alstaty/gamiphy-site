
<?php
/**
* Template Name: GAM Features  
 *
 */
 get_header('inner'); ?>

   <!--=================== content area ================================== -->
    <section class="gamibot" id="Drive">
      <div class="container ">
        <div class="hed-left col-md-6">
              
              
              <?php if( have_rows('header_content') ): ?>

              

                <?php while( have_rows('header_content') ): the_row(); 

                  // vars
                  $icon = get_sub_field('icon');
                  $icon_title = get_sub_field('icon_title');
                  $image = get_sub_field('image');
                  $content = get_sub_field('content');
                  $demo_link  = get_sub_field('demo_link');
                  $demo_link1  = get_sub_field('demo_link1');
                  $link= get_sub_field('link ');
                  $buttontitle = get_sub_field('label');
                  $buttontitle_j = get_sub_field('label_1');

                  ?>

                  <h4>
                  <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" / >
                  <?php echo $icon_title; ?>
                  </h4>

                  <?php echo $content; ?>
                  <div class="btn-wrap">
                    <?php if( $demo_link ): ?>
                      <a href="<?php echo $demo_link; ?>" class="btn button-1"><?php echo $buttontitle; ?></a>
                    <?php endif; ?>
                    
                    <?php if( $demo_link1 ): ?>
                      <a href="<?php echo $demo_link1; ?>" class="btn button-2"> <?php echo$buttontitle_j; ?></a>
                    <?php endif; ?>
                   
                  </div>

            </div>
            <div class="hed-right col-md-5 pull-right text-right">
                <div class="row">
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" / >
                </div>
            </div>

             <?php endwhile; ?>

              <?php endif; ?>

        </div>

    </div>
  </section>
  
   <?php if( have_rows('gami_repeat') ): ?>

              <?php while( have_rows('gami_repeat') ): the_row(); 

                // vars
                $icon = get_sub_field('icon');
                $icon_title = get_sub_field('icon_title');
                $image = get_sub_field('image');
                $content = get_sub_field('content');
                //$small_data = get_sub_field('small_data');
                ?>

  <section class="gami-repeat" id="Engage">
      <div class="container">
          <div class="col-md-6 left-gimg">              
            <div id="engageslide" class="carousel slide" data-ride="carousel"  data-interval="3000">
               <!-- Wrapper for slides -->
                <div class="carousel-inner">
                

                  <?php if( have_rows('engage_slides') ): ?>
                
                  <?php 
                  $slides =    get_sub_field('slides');
                    $sl =0 ;
                    while( have_rows('engage_slides') ): the_row();

                    $sl++;

                    ?>
                    <div class="item <?php if ($sl==1) {?> active <?php } ?>">
                       <img src="<?php the_sub_field('slides'); ?>" alt="" / >
                    </div>

                    <?php  endwhile; ?>
                    <?php  endif; ?>

                
                </div>
                <!-- Indicators -->
                <ol class="carousel-indicators">
                  <li data-target="#engageslide" data-slide-to="0" class="active"></li>
                  <li data-target="#engageslide" data-slide-to="1"></li>
                  <li data-target="#engageslide" data-slide-to="2"></li>
                  <li data-target="#engageslide" data-slide-to="3"></li>
                </ol>
                <div class="man-image">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" / >
                </div>

            </div>
          </div>

          <div class="col-md-5 right-content">

               <h4>
                  <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" / >
                  <?php echo $icon_title; ?>
                  </h4>

             <?php echo $content; ?>

              <?php if( have_rows('small_data') ): ?>
              
              <?php 

                while( have_rows('small_data') ): the_row();


                  ?>
                  <div class="col-md-6">
                       <div class="iconimg">
                        <img src="<?php the_sub_field('small_image'); ?>" alt="" >
                       </div>
                      <h6 class="icon-tit"><?php the_sub_field('small_title'); ?></h6>
                      <span class="icon-sub"><?php the_sub_field('small_decs'); ?></span>
                   </div>

                <?php endwhile; ?>
                <?php endif; ?>

              <p class="text-left"><a href="https://gamiphy.co/contact-us-games/" class="learn-more">Explore our Games library <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-right-copy.png" alt="arrow"></a></p>

              </div>

          </div>
      </div>   
  </section>

    <?php endwhile; ?>

   <?php endif; ?>

    <?php if( have_rows('collect_data') ): ?>

              <?php while( have_rows('collect_data') ): the_row(); 

                // vars
                $icon = get_sub_field('icon');
                $icon_title = get_sub_field('icon_title');
                $image = get_sub_field('image');
                $content = get_sub_field('content');
                ?>

  <section class="gami-repeat rightbg" id="Collect">
       <div class="container">
        <div class="col-md-5 left-content">
            <h4>
            <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" / >
            <?php echo $icon_title; ?>
            </h4>
                <?php echo $content; ?>
           </div>
           <div class="col-md-6 right-gimg">
             <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" / >
          </div>
      </div>
  </section>

 <?php endwhile; ?>

  <?php endif; ?>

   <?php if( have_rows('more_gamification') ): ?>

             

  <section class="gami-feed" id="Connecting ">
       <div class="container">
         <?php while( have_rows('more_gamification') ): the_row(); 
                // vars
                $content = get_sub_field('content');
                ?>
           <div class="col-md-8 feed-content  mx-auto">
              <?php echo $content; ?>
          </div>

           <div class="col-md-12 feed-image">
              <div class="row">

                      <?php if( have_rows('feed_data') ): ?>
              
              <?php 

                while( have_rows('feed_data') ): the_row();
                $icon = get_sub_field('icon');
                $label = get_sub_field('label');
                $image = get_sub_field('image');

                  ?>

                   <div class="col-md-4">
                      <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" />
                      <p><?php echo $label; ?></p>
                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                  </div>

                   <?php endwhile; ?>

                   <?php endif; ?>

               </div>
           </div>

       </div>
   </section>

 <?php endwhile; ?>

  <?php endif; ?>


  <?php if( have_rows('motivate_consumers') ): ?>


  <section class="gami-repeat motivate_consumers" id="Motivate">
       <div class="container">

              <?php while( have_rows('motivate_consumers') ): the_row(); 

                // vars
                $icon = get_sub_field('icon');
                $icon_title = get_sub_field('icon_title');
                $image = get_sub_field('image');
                $content = get_sub_field('content');
                ?>

           <div class="col-md-6 left-gimg">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
           </div>
           <div class="col-md-5 right-content">               
               <div class="row icon-mini" >
                <h4>
                <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" / >
                <?php echo $icon_title; ?>
                </h4>
                <?php echo $content; ?>

                  <?php if( have_rows('mini_icon') ): ?>
               <ul class="list-inline">
                <?php 

                while( have_rows('mini_icon') ): the_row();
                 $icon = get_sub_field('icon');


                  ?>
                  <li><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" /></li>
                <?php endwhile; ?>
                </ul>
              <?php endif;?> 
                   
               </div>
              <p><a href="https://gamiphy.co/contact-us-badges/" class="learn-more">Explore our special badges <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-right-copy.png" alt="arrow"></a></p>
           </div>
           <?php endwhile; ?>
       </div>

   </section>

  <?php endif;?> 
   
     <?php if( have_rows('full_control') ): ?>

   <section class="full-control" id="Fullcontrol">
       <div class="container">
        <?php while( have_rows('full_control') ): the_row(); 
                
                // vars
                $heading = get_sub_field('heading');
                $image = get_sub_field('image');
                $content = get_sub_field('content');
                ?>
           <div class="col-md-12 text-center ">
               <h2><?php echo $heading; ?></h2>
           </div>
           
           <div class="row">
           <div class="col-md-4 control-tab">
             

               <ul class="nav nav-tabs">

                <?php if( have_rows('side_tab') ): ?>

                <?php 
                $count = 1;
                while( have_rows('side_tab') ): the_row();
                
                 $icon = get_sub_field('icon');
                 $label= get_sub_field('label');
                 $excerpt= get_sub_field('excerpt');

                  ?>
                  <li  class="<?php if ($count == 1) {?> active <?php } ?>">
                  <a data-toggle="tab" href="#menu-<?php echo $count; ?>" class="tick">

                      <div class="icontab">
                        <img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" / >
                      </div>
                      <span><?php echo $label; ?></span>
                      <p class="tab-desc"><?php echo $excerpt; ?></p>
                   </a>

                   </li>
                <?php  $count++; endwhile; ?>
                </ul>
              <?php endif;?> 

               </div>
               <div class="col-md-8 control-content">
                   <div class="tab-content">
                   
                    <?php if( have_rows('big_thumbnail') ): ?>
                      <?php 
                      $count1 = 1;
                      while( have_rows('big_thumbnail') ): the_row();
                       $image = get_sub_field('image');
                       
                     
                        ?>
                     <div id="menu-<?php echo $count1; ?>" class="tab-pane fade  <?php if ($count1 == 1) {?> in active <?php }?>">
                      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" class="img-responsive" />
                    </div>
                     <?php $count1++;  endwhile; ?>

                     <?php endif;?>
                   
                   </div>
                       
               </div>
                <?php endwhile; ?>
           </div>
           <div class="clearfix"></div>

            <?php if( have_rows('interesting') ): ?>

              <?php while( have_rows('interesting') ): the_row(); 

                // vars
                $heading = get_sub_field('heading');
                $label = get_sub_field('label');
                $link = get_sub_field('link');
                ?>
           <div class="col-md-12 interesting-text ">

               <h3 class="interesting"><?php echo $heading ?></h3>
               <a href="<?php echo $link ?>" class="btn button-2 try-it-now "><?php echo $label ?></a>
           </div>

           <?php endwhile; ?>

            <?php endif;?>


      </div>
   </section>

<?php endif;?> 

<?php if( have_rows('unlock_our_premium_plan') ): ?>
  <section class="journey">
    <div class="container">
      <div class="col-md-12">
      


            <?php while( have_rows('unlock_our_premium_plan') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content  = get_sub_field('content');
              $label  = get_sub_field('label');
              $link  = get_sub_field('link');

              ?>

          <div class="col-md-8">
            <h2><?php echo $content; ?></h2>
            <div class="btn-wrap">
            <?php if( $link ): ?>
              <a href="<?php echo $link; ?>" class="btn button-2"><?php echo $label; ?>
              <img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
              <?php endif; ?>
            </div>
          </div>
          <div class="col-md-3 ">
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
          </div>
        <?php endwhile; ?>

              
      </div>
    </div>
  </section>
  <?php endif; ?>

  <?php if( have_rows('customers_journey') ): ?>
  <section class="journey">
    <div class="container">
      <div class="col-md-12">
      


            <?php while( have_rows('customers_journey') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $label  = get_sub_field('label');

              ?>

        <div class="col-md-3">
          <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
        </div>
        <div class="col-md-8">
          <h2><?php echo $title; ?></h2>
          <div class="btn-wrap">
          <?php if( $link1 ): ?>
          <a href="<?php echo $link1; ?>" class="btn button-2"><?php echo $label; ?></a>
          <?php endif; ?>
        </div>
        </div>
        <?php endwhile; ?>

      </div>
    </div>
  </section>
  <?php endif; ?>


<?php get_footer(); ?>

