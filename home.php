<?php
/**
 * Template Name: GAM home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 */

get_header(); ?>

	<!--=================== content area ================================== -->

	<div class="container">
		<?php if( have_rows('celebration') ): ?>


            <?php while( have_rows('celebration') ): the_row();

              // vars
              $image = get_sub_field('image');
              $content = get_sub_field('content');
              $title  = get_sub_field('title');
              $sub_title  = get_sub_field('sub_title');
              $link  = get_sub_field('link');

              ?>

			<div class=" col-xs-12 promotions">
				<div class="col-md-4 promotions-left">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8 promotions-right">

					<?php echo do_shortcode('[wp-video-popup video="'.$link.'"]') ?>

					<a href="#"  class="wp-video-popup">
						<div class="Oval">
							<div class="Rectangle">&#9658;</div>
						</div>
					</a>
					<p class="your-brand-to wp-video-popup"><?php echo $title; ?></p>
					<p class="Play-Video wp-video-popup"><?php echo $sub_title; ?></p>
				</div>
			</div>
		</div>

		<?php endwhile; ?>

	    <?php endif; ?>

	    </div>


	<?php if( have_rows('content') ): ?>
    <?php while( have_rows('content') ): the_row();

      // vars
      $image = get_sub_field('image');
      $editor  = get_sub_field('editor');
      $label  = get_sub_field('label');
      $link  = get_sub_field('link');

      ?>

	<section class="takeactions">
			<div class="col-inline ">
				<?php echo $editor; ?>
				<p>
				<a href="<?php echo site_url(); ?><?php echo $link; ?>" class="learn-more"> <?php echo $label; ?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/arrow-right-copy.png" alt="arrow">
				</a>
				</p>
			</div>
			<div class="col-inline bg-img" style=" background-image: url('<?php echo $image['url']; ?>'); "></div>


	</section>

	<?php endwhile; ?>
	<?php endif; ?>


	<div class="container">
		<div class="col-md-12 leading">

			<?php  $leading_brand_content = get_field('leading_brand_content' );

			echo $leading_brand_content;
			 ?>


			<?php if( have_rows('leading_brand') ): ?>

			<ul class="list-inline">
				 <?php while( have_rows('leading_brand') ): the_row();
				   $image = get_sub_field('image');
				   ?>
				<li><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></li>

				<?php endwhile; ?>
			</ul>

          	<?php endif; ?>

		</div>
	</div>

	<section class="hellogamibot">
		<div class="container">
			<div class="col-md-12">
				<?php if( have_rows('hello_gamibot') ): ?>

		        <?php while( have_rows('hello_gamibot') ): the_row();
					$small_text = get_sub_field('small_text');
		            $big_text  = get_sub_field('big_text');
		        ?>
				<h2 class="text-center"><span class="Hello"><?php echo $small_text ;?></span><?php echo $big_text ;?></h2>
				<?php endwhile; ?>
				<?php endif; ?>


				<?php if( have_rows('gamibot_plans') ): ?>


		        <?php $count= 1 ;  while( have_rows('gamibot_plans') ): the_row();
					$content = get_sub_field('content');
		            $link  = get_sub_field('link');
		            $image  = get_sub_field('image');
		            $unlock_image  = get_sub_field('unlock_image');
		            $unlock  = get_sub_field('unlock');
		            $over_heading  = get_sub_field('over_heading');
		            $over_image  = get_sub_field('over_image');
		            $lock_image  = get_sub_field('lock_image');
		            $lock  = get_sub_field('lock');


		              ?>

					<div class="col-md-6 gamiin-cover">

						<div class="gamiin"  style="overflow:hidden">
						<div class="Mask" style="background-image: url(<?php echo $image['url'];?>);display:none"></div>
						  <div  class="gamiover
						<?php if ($count == 1) {?>
						one
						<?php }?>

						<?php if ($count == 2) {?>
						two
						<?php }?>

						<?php if ($count == 3) {?>
						three
						<?php }?>
						<?php if ($count == 4) {?>
						four
						<?php }?>

						">
							<h3><?php echo $over_heading ;?></h3>
							<div class="overdp" style="background-image: url(<?php echo  $over_image['url'] ;?>);" ></div>

							<div class="lock" style="background-image: url(<?php echo  $lock_image['url'] ;?>);">
								<?php echo $lock ;?>
							</div>
						</div>

						<?php echo $content ;?>
						<a href="<?php echo $link ;?>" class=" clr-<?php echo $count;?>">
							<div class="Unlock" style="background-image: url(<?php echo  $unlock_image['url'] ;?>);">
								<span class=" clr-<?php echo $count;?>"><?php echo $unlock ;?></span>
							</div>
						</a>
						</div>

					</div>

				<?php $count++;  endwhile; ?>
			</div><?php endif; ?>

		</div>
	</section>

	<?php if( have_rows('unlock_our_premium_plan') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">



            <?php while( have_rows('unlock_our_premium_plan') ): the_row();

              // vars
              $image = get_sub_field('image');
              $content  = get_sub_field('content');
              $label  = get_sub_field('label');
              $link  = get_sub_field('link');

              ?>

					<div class="col-md-8">
						<h2><?php echo $content; ?></h2>
						<div class="btn-wrap">
						<?php if( $link ): ?>
							<a href="<?php echo $link; ?>" class="btn button-2"><?php echo $label; ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-3 ">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					</div>
				<?php endwhile; ?>


			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if( have_rows('customers_journey') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">



            <?php while( have_rows('customers_journey') ): the_row();

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $label  = get_sub_field('label');

              ?>

				<div class="col-md-3">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8 text-center">
					<h2><?php echo $title; ?></h2>
					<div class="btn-wrap">
					<?php if( $link1 ): ?>
					<a href="<?php echo $link1; ?>" class="btn button-2"><?php echo $label; ?></a>
					<?php endif; ?>
				</div>
				</div>
				<?php endwhile; ?>

			</div>
		</div>
	</section>
	<?php endif; ?>


	<!--=================== content area end ================================== -->

<?php get_footer(); ?>

<script type="text/javascript">

( function($) {
  $(".gamiover").click(function() {
    $(this).css("transform", "scale(0)");
    $(this).parent().css("overflow", "visible");
    $(this).siblings().css("display", "block");

  });
} ) ( jQuery );
</script>
