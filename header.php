<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?>

<?php get_template_part( 'parts/head'); ?>

<header class="header-custom">
	<div class="header-wrap">
	<div class="container">

		<?php get_template_part( 'parts/nav'); ?>
    <?php get_template_part( 'parts/navmb'); ?>

  

        <div class="hed-left col-md-6">
        	<?php if ( is_front_page() && !is_home() ) { ?>
          <h2 class="gamiboth">Gamibot <span class="TM">TM</span> <span class="by-Gamiphy">by Gamiphy</span></h2>
          <?php } ?>
        	<?php if( have_rows('header_content') ): ?>


            <?php while( have_rows('header_content') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content = get_sub_field('content');
              $demo_link  = get_sub_field('demo_link');
              $demo_link1  = get_sub_field('demo_link1');
              $link= get_sub_field('link ');
              $buttontitle = get_sub_field('label');
              $buttontitle_j = get_sub_field('label_1');

              ?>

              <?php echo $content; ?>
              <div class="btn-wrap">
                <?php if( $demo_link ): ?>
                  <!-- <a href="<?php echo $demo_link; ?>" class="btn button-1"><?php// echo $buttontitle; ?></a> -->
                <?php endif; ?>
                
                <?php if( $demo_link1 ): ?>
                  <a href="<?php echo $demo_link1; ?>" class="btn button-2"> <?php echo$buttontitle_j; ?></a>
                <?php endif; ?>
               
              </div>

        </div>
        <div class="hed-right col-md-5  <?php if ( is_front_page() && !is_home() ) { ?> text-right <?php } ?>">
            <div class="row">
            <img src="<?php echo $image['url']; ?>"  alt="<?php echo $image['alt'] ?>" />
            </div>
        </div>

         <?php endwhile; ?>

          <?php endif; ?>

    </div>

</header>
<div class="clearfix"></div>

<body <?php body_class(); ?>>
