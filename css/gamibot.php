
<?php
/**
* Template Name: GAM gamibot  
 *
 */
 get_header('inner'); ?>

   <!--=================== content area ================================== -->
      <section class="gamibot">
       <div class="container ">
          <div class="hed-left col-md-5">
           <img src="<?php echo get_template_directory_uri(); ?>/images/quests.png">
           
           
           
             <h2>Drive user behavior with Quests</h2>
<p>Leave no customer behind. Reward each customer while he/she embarks on the journey you designed to meet your business goals.</p>
                <div class="btn-wrap">
                                      
                                      
               </div>

          </div>
          <div class="hed-right col-md-5 text-right">
              <div class="row">
             <img src="<?php echo get_template_directory_uri(); ?>/images/gamibot-quests-combined.png" alt="">
             </div>
         </div>

            
      </div>
  
   </section>
  
  <section class="gami-repeat">
      <div class="container">
          <div class="col-md-6 left-gimg">               <img src="<?php echo get_template_directory_uri(); ?>/images/gamibot-games-combined.png" alt=""
               >
          </div>
          <div class="col-md-5 right-content">
               <img src="<?php echo get_template_directory_uri(); ?>/images/games.png" alt=""
             >
              <h2>Engage and convert more users with Games</h2>
              <p>Leave no customer behind. Reward each customer while he/she embarks on the journey you designed to meet your business goals.</p>              <div class="row icon-mini" >
                   <div class="col-md-6">
                       <div class="iconimg">
                           <img src="<?php echo get_template_directory_uri(); ?>/images/catch-the-cat.png" alt="">
                       </div>
                       <h6 class="icon-tit">Catch the Cat</h6>
                      <span class="icon-sub">Action</span>
                   </div>
                   <div class="col-md-6">
                       <div class="iconimg">
                          <img src="<?php echo get_template_directory_uri(); ?>/images/iqos-santa.png" alt="">
                      </div>
                      <h6 class="icon-tit">Hello Santa</h6>
                      <span class="icon-sub">Fantasy</span>                   </div>
<div class="col-md-6">
<div class="iconimg">
                         <img src="<?php echo get_template_directory_uri(); ?>/images/mouse-planet.png" alt="">
                       </div>
                       <h6 class="icon-tit">Mouse Planet</h6>                       <span class="icon-sub">Adventureãƒ»Fantasy</span>
                  </div>
                   <div class="col-md-6">
                       <div class="iconimg">
                           <img src="<?php echo get_template_directory_uri(); ?>/images/iqos-halloween.png" alt="">
                       </div>
<h6 class="icon-tit">Halloween</h6>                      <span class="icon-sub">Horror</span>
                  </div>
              </div>
              <p><a href="<?php the_permalink();?>" class="learn-more">Explore our Games library <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-right-copy.png" alt="arrow"></a></p>

          </div>
      </div>   </section>

  <section class="gami-repeat rightbg">
       <div class="container">
        <div class="col-md-5 left-content">
           <img src="<?php echo get_template_directory_uri(); ?>/images/quizzes.png">
               <h2>Collect data and showcase products with Quizzes</h2>
               <p>Leave no customer behind. Reward each customer while he/she embarks on the journey you designed to meet your business goals.</p>
           </div>
           <div class="col-md-6 right-gimg">
              <img src="<?php echo get_template_directory_uri(); ?>/images/gamibot-quizzes-combined.png">
          </div>
      </div>
  </section>


  

  <section class="gami-feed">
       <div class="container">
           <div class="col-md-8 feed-content  mx-auto">
              <h2>More Gamification Happier Customers</h2>
               <p>Leave no customer behind. Reward each customer while he/she embarks on the journey
              you designed to meet your business goals.</p>
          </div>
           <div class="col-md-12 feed-image">
              <div class="row">
                   <div class="col-md-4">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/leader-boardic.png" alt="">
                      <p>Leader Board</p>
                       <img src="<?php echo get_template_directory_uri(); ?>/images/leader-board.png" alt="">
                  </div>
                  <div class="col-md-4">
                     <img src="<?php echo get_template_directory_uri(); ?>/images/live-feedic.png" alt="">
                      <p>Live Feed</p>
                      <img src="<?php echo get_template_directory_uri(); ?>/images/live-feed.png" alt="">
                   </div>
                   <div class="col-md-4">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/rewardsic.png" alt="">
                       <p>Rewards</p>
                     <img src="<?php echo get_template_directory_uri(); ?>/images/rewards.png" alt="">
                   </div>
               </div>
           </div>
       </div>
   </section>

  <section class="gami-repeat">
       <div class="container">
           <div class="col-md-6 left-gimg">
               <img src="<?php echo get_template_directory_uri(); ?>/images/special-badges.png" alt="">
           </div>
           <div class="col-md-5 right-content">
               <img src="<?php echo get_template_directory_uri(); ?>/images/badges.png" alt="">
               <h2>Motivate consumers with special badges and social status</h2>
               <p>Leave no customer behind. Reward each customer while he/she embarks on the journey you designed to meet your business goals.</p>              <div class="row icon-mini" >
                   <ul class="list-inline">
                     <li><img src="<?php echo get_template_directory_uri(); ?>/images/badge-1.png" alt=""></li>
                       <li><img src="<?php echo get_template_directory_uri(); ?>/images/badge-2.png" alt=""></li>
    <li><img src="<?php echo get_template_directory_uri(); ?>/images/badge-3.png" alt=""></li>                      <li><img src="<?php echo get_template_directory_uri(); ?>/images/badge-4.png" alt=""></li>
                       <li><img src="<?php echo get_template_directory_uri(); ?>/images/badge-5.png" alt=""></li>
                       <li><img src="<?php echo get_template_directory_uri(); ?>/images/badge-6.png" alt=""></li>
                       <li><img src="<?php echo get_template_directory_uri(); ?>/images/badge-7.png" alt=""></li>
                      <li><img src="<?php echo get_template_directory_uri(); ?>/images/badge-8.png" alt=""></li>
                   </ul>
               </div>
              <p><a href="<?php the_permalink();?>" class="learn-more">Explore our Games library <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-right-copy.png" alt="arrow"></a></p>
           </div>
       </div>
   </section>
   <section class="full-control">
       <div class="container">
           <div class="col-md-12 text-center ">
               <h2>You have full control</h2>
           </div>
           
           <div class="row">
               <div class="col-md-4 control-tab">
                   <ul class="nav nav-tabs">
                    <li class="active">
                      <a data-toggle="tab" href="#home">
                      <div class="icontab">
                       <img src="<?php echo get_template_directory_uri(); ?>/images/brandingic.png" alt=""></div>                       <span>Home</span>
                       <p class="tab-desc">Brand it, adjust colors, position,
                           look and feel so it matches
                           and adds to your store.
                       </p>
                       </a>
                     </li>
                     <li><a data-toggle="tab" href="#menu1">
                     <div class="icontab">
                     <img src="<?php echo get_template_directory_uri(); ?>/images/levels.png" alt=""></div>
                       <span>Levels, Quests, Points,
                           Rewards and More!</span>
                       <p class="tab-desc">
                       </p>
                       </a></li>
                     <li><a data-toggle="tab" href="#menu2"><div class="icontab">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/games.png" alt=""></div>
                      <span>Build and customize
                           branded games in minutes</span>                       <p class="tab-desc">
                       </p>
                      </a></li>
                      <li><a data-toggle="tab" href="#menu3"><div class="icontab">
                       <img src="<?php echo get_template_directory_uri(); ?>/images/track.png" alt=""></div>
                       <span>Track and analyze user
                               behavior, Act fast..
                       </span>
                       <p class="tab-desc">
                       </p>
                       </a></li>
                  </ul>
               </div>
               <div class="col-md-8 control-content">
                   <div class="tab-content">
                     <div id="home" class="tab-pane fade in active">
                      <img src="<?php echo get_template_directory_uri(); ?>/images/branding.png" alt=""  class="img-responsive">
                    </div>
                     <div id="menu1" class="tab-pane fade">
                      <h3>Levels, Quests, Points,
                         Rewards and More!</h3>
                      <p>Some content.</p>
                   </div>
                    <div id="menu2" class="tab-pane fade">
                      <h3>Build and customize
                           branded games in minutes</h3>
                       <p>Some content.</p>
                     </div>
                      <div id="menu3" class="tab-pane fade">
                       <h3>Track and analyze user
                               behavior, Act fast..</h3>
                       <p>Some content </p>
                     </div>
                   </div>
                       
               </div>
           </div>
           <div class="clearfix"></div>
           <div class="col-md-12 interesting-text ">
               <h3 class="interesting">Interesting?</h3>
               <a href="#" class="btn button-2 try-it-now ">Try it now</a>
           </div>

           <div class="col-md-12 text-center ">
               <h3 class="Do-you-have">Do you have what it takes to unlock our premium plan?</h3>
               <p><a href="#" class="learn-more text-center">Enter the Gami-bot challenge  <img src="http://localhost/wordpress/wp-content/themes/gamiphy-site/images/arrow-right-copy.png" alt="arrow"></a></p>
           </div>


      </div>
   </section>



    <section class="journey">
        <div class="container">
            <div class="col-md-12">
            <?php if( have_rows('customers_journey') ): ?>


            <?php while( have_rows('customers_journey') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $link  = get_sub_field('link1');

              ?>

                <div class="col-md-3">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                </div>
                <div class="col-md-8">
                    <h2><?php echo $title; ?></h2>
                    <div class="btn-wrap">
                    <?php if( $link1 ): ?>
                    <a href="<?php echo $link1; ?>" class="btn button-1">Request demo</a>
                    <?php endif; ?>
                    <?php if( $link ): ?>
                    <a href="<?php echo $link; ?>" class="btn button-2">Begin the journey</a>
                    <?php endif; ?>
                </div>
                </div>
                <?php endwhile; ?>

                <?php endif; ?>
            </div>
        </div>
    </section>

 

<?php get_footer(); ?>
