<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 */
global $post;
$categories = get_the_category();

?>
	
	<header class="entry-header">
		<a href="<?php echo site_url(); ?>/blog" class="back"><img src="<?php echo get_template_directory_uri(); ?>/images/back.png" alt=""> Back to Blog</a>
		<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
		<div class="singleuser">
		<span class="img-user-single">
		<?php echo get_avatar( $author_id ) ; ?>
		</span>
		 <span class="clr"><?php echo get_the_author_meta('display_name', $author_id); ?></span>
		 <span>Posted on </span><span class="clr"><?php  echo get_the_date(); ?> </span>
		 </div>
	</header><!-- .entry-header -->
	
	<div class="feature-banner col-md-12" >
				 
				<?php
				// Must be inside a loop.
				 
				//if ( has_post_thumbnail() ) {?>
				    
				 <?php  //$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>

				   <!-- <div class="gradient <?php // echo $categories[0]->slug; ?>" style="background-image: url(<?php //echo  $featured_img_url; ?>);background-size: cover;background-position: center center;"></div> -->

				    <?php //}?>

				   <?php  //if (!has_post_thumbnail() ) {?>

					    <div class="gradient <?php  echo $categories[0]->slug; ?>"></div>
					<?php //}?>

	</div>

<div class="row">

	<article id="post-<?php the_ID(); ?>"class="col-md-8 single-content">
		
		<?php twentysixteen_post_thumbnail(); ?>

		<?php the_content(); ?>

		

		<div class="entry-content">
			<?php
				//the_content();

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );

				if ( '' !== get_the_author_meta( 'description' ) ) {
					get_template_part( 'template-parts/biography' );
				}
			?>
		</div><!-- .entry-content -->
		<div class="comments-box">
		<?php
		// Start the loop.

			// Include the single post content template.
			//get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template('/comments.php',true);
			}
			
			// End of the loop.
		?>

	</div>	
		
	</article><!-- #post-## -->

	
	<aside class="pull-right single-side col-md-3">
		<img src="<?php echo get_template_directory_uri(); ?>/images/celebration.png" alt="">
		<h3>SUBCRIBE TO OUR Newsletter</h3>
		<div class="clearfix"></div>
		<?php echo do_shortcode('[mailpoet_form id="1"]'); ?>
	</aside>

	

</div>



<div class="Related-Articles">
	<div class="row">
			<h3 class="relatedtitle">Related Articles</h3>

			<?php $related_args = array(
				'post_type' => 'post',
				'posts_per_page' => 3,
			);
			$related = new WP_Query( $related_args );

			if( $related->have_posts() ) :
			?>
			
			<?php while( $related->have_posts() ): $related->the_post(); $categories = get_the_category(); ?>

			<div class="col-md-4 <?php  echo $categories[0]->name; ?>" >
					<article class="post-art ">
						<a href="<?php the_permalink(); ?>">
							<div class="image-post">
								<div class="gradient <?php  echo $categories[0]->slug; ?>"></div>
							</div>
						</a>
						<div class="art-content">			
							<h3 class="art-title">
							<a href="<?php the_permalink(); ?>" class="art-title"><?php the_title(); ?></a>
							</h3>
							<?php the_content(); ?>
						</div>

						<div class="meta-blog">
							<div class="usertit">🤖 <?php echo get_the_author_meta('display_name', $author_id); ?></div>
							<div class="comment-count">
								<span><?php printf( _nx( '1', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( get_comments_number() ) ); ?></span>
								<img src="<?php echo get_template_directory_uri(); ?>/images/comments-copy.svg">
							</div>
						</div>
					</article>
			</div>
			<?php endwhile; ?>
			<?php
			endif;
			wp_reset_postdata() ?>

	</div>
</div>

