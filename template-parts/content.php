<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 */
global $post;
$categories = get_the_category();
?>
	


<div class="col-md-4 <?php  echo $categories[0]->name; ?>" >
<article class="post-art ">
		<a href="<?php the_permalink(); ?>">
			<div class="image-post">

				 
				<?php
				// Must be inside a loop.
				 
				if ( has_post_thumbnail() ) {?>
				    
				 <?php  $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>

				   <div class="gradient <?php  echo $categories[0]->slug; ?>" style="background-image: url(<?php echo  $featured_img_url; ?>);background-size: cover;background-position: center center;"></div>

				    <?php }?>

				   <?php  if (!has_post_thumbnail() ) {?>

					    <div class="gradient <?php  echo $categories[0]->slug; ?>"></div>
					<?php }?>

			</div>
		</a>
		<div class="art-content">			
			<h3 class="art-title">
			<a href="<?php the_permalink(); ?>" class="art-title"><?php the_title(); ?></a>
			</h3>
			<?php the_content(); ?>
		</div>

		<div class="meta-blog">
			<div class="usertit">🤖 <?php echo get_the_author_meta('display_name', $author_id); ?></div>
			<div class="comment-count">
				<span><?php printf( _nx( '1', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( get_comments_number() ) ); ?></span>

				<img src="<?php echo get_template_directory_uri(); ?>/images/comments-copy.svg">
			</div>
		</div>
	</article>

</div>