<?php
/**
 * Template Name: GAM blog  
 *
 */
get_header('inner'); ?>

<div id="primary" class="container" style="min-height:600px;">
	<main id="main" class="site-main col-md-12" role="main">
		<div class="row blog-head">
			<?php if( have_rows('header_content') ): ?>
            <?php while( have_rows('header_content') ): the_row(); 

              // vars
              $left_image = get_sub_field('left_image');
              $right_image  = get_sub_field('right_image');
              $heading  = get_sub_field('heading');
              $sub_heading  = get_sub_field('sub_heading');

              ?>

			<div class="col-md-2 col-xs-6">
			<img src="<?php echo $left_image['url']; ?>" alt="<?php echo $left_image['alt'] ?>" />
			</div>
			<div class="col-md-8 contentblog col-xs-6">
			<h2><?php echo $heading; ?></h2>
			<p><?php echo $sub_heading; ?></p>
			</div>
			<div class="col-md-2 col-xs-6">
			<img src="<?php echo $right_image['url']; ?>" alt="<?php echo $right_image['alt'] ?>" />
			</div>

			<?php endwhile; ?>
			<?php endif; ?>
		</div>
		<div class="row">
			<div class="col-md-4">
				<aside class="blog-side">
					<h3>Browse by Topics</h3>
					<ul>
					<?php  $categories = get_categories();
						foreach($categories as $category) {
						   echo '<li class='. esc_html($category->name) . '><a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a></li>';
						}
					?>
					</ul>
				</aside>
			</div>

	
	<div class="col-md-8">
		<div class="row">
			<?php			 
			 $recent_posts = wp_get_recent_posts(
				array(
		        'posts_per_page' => 2,
		        'offset'         => 1,
		        'order' => 'ASC',
		    ));


			foreach( $recent_posts as $recent ){?>
				<?php 
				$categories = get_the_category($recent["ID"]);
				// $post_id       = $recent['ID'];
			 //    $post_url       = get_permalink($recent['ID']);
			 //    $post_title     = $recent[post_title];
			     $post_content   = $recent['post_content'];
			 //    $post_thumbnail = get_the_post_thumbnail($recent['ID']);
			     ?>
				

				<div class="col-md-6 <?php  echo $categories[0]->name; ?>" >
					<article class="post-art ">
						<a href="<?php the_permalink($recent["ID"]); ?>">
							<div class="image-post">
								<div class="gradient <?php  echo $categories[0]->slug; ?>"></div>
							</div>
						</a>
						<div class="art-content">			
							<h3 class="art-title">
							<a href="<?php the_permalink($recent["ID"]); ?>" class="art-title"><?php the_title($recent["post_title"]); ?></a>
							</h3>
							<p><?php echo $post_content; ?></p>
						</div>

						<div class="meta-blog">
							<div class="usertit">🤖<?php the_author_meta( 'display_name', $recent['post_author'] ); ?></div>
							<div class="comment-count">
								<span><?php printf( _nx( '1', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( get_comments_number() ) ); ?></span>
								<img src="<?php echo get_template_directory_uri(); ?>/images/comments-copy.svg">
							</div>
						</div>
					</article>
				</div>
				<?php }
					wp_reset_query();
				?>	
			</div>
		</div>

		</div>

	</main>
	
	<?php if( have_rows('celebration') ): ?>



            <?php while( have_rows('celebration') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content = get_sub_field('content');
              $title  = get_sub_field('title');
              $sub_title  = get_sub_field('sub_title');
              $link  = get_sub_field('link');

              ?>

			<div class="col-md-10 promotions pro-blog col-md-offset-1">
				<div class="col-md-4 promotions-left">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8 promotions-right ">
					<h3 class="SUBCRIBE-TO-OUR-NEWL"> <?php echo $title; ?></h3>
					<div class="subscribe-form">

					<?php echo do_shortcode('[mailpoet_form id="1"]'); ?>
					</div>
				</div>
			</div>
		</div>

		<?php endwhile; ?>

	    <?php endif; ?>
</div>


<section class="post-looping">
	
	<div class="container">
		<div class="row">
		
		<?php
		    
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			$args = array(
			    'post_type'=>'post', // Your post type name
			    'posts_per_page' => 3,
			    'paged' => $paged,
			);

			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
			    while ( $loop->have_posts() ) : $loop->the_post();

        	$categories = get_the_category(); 
        	
        ?>

            <div class="col-md-4 <?php  echo $categories[0]->name; ?>" >
					<article class="post-art ">
						<a href="<?php the_permalink(); ?>">
							<div class="image-post">
								<div class="gradient <?php  echo $categories[0]->slug; ?>"></div>
							</div>
						</a>
						<div class="art-content">			
							<h3 class="art-title">
							<a href="<?php the_permalink(); ?>" class="art-title"><?php the_title(); ?></a>
							</h3>
							<?php the_content(); ?>
						</div>

						<div class="meta-blog">
							<div class="usertit">🤖 <?php echo get_the_author_meta('display_name', $author_id); ?></div>
							<div class="comment-count">
								<span><?php printf( _nx( '1', '%1$s Comments', get_comments_number(), 'comments title', 'textdomain' ), number_format_i18n( get_comments_number() ) ); ?></span>

								<img src="<?php echo get_template_directory_uri(); ?>/images/comments-copy.svg">
							</div>
						</div>
					</article>
			</div>

	       <?php endwhile; 
	      
		    $total_pages = $loop->max_num_pages;

		    if ($total_pages > 1){

		        $current_page = max(1, get_query_var('paged'));

		        echo "<div class='pagination col-md-12'>";
		        
		        echo paginate_links(array(
		            'base' => get_pagenum_link(1) . '%_%',
		            'format' => '/page/%#%',
		            'current' => $current_page,
		            'total' => $total_pages,
		            'prev_text'    => __('prev'),
		            'next_text'    => __('next'),
		        ));

		        echo "</div>";

		    }    
		}
		wp_reset_postdata();?>
				
				
		</div>
	</div>
</section>

<?php if( have_rows('unlock_our_premium_plan') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('unlock_our_premium_plan') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content  = get_sub_field('content');
              $label  = get_sub_field('label');
              $link  = get_sub_field('link');

              ?>

					<div class="col-md-8">
						<h2><?php echo $content; ?></h2>
						<div class="btn-wrap">
						<?php if( $link ): ?>
							<a href="<?php echo $link; ?>" class="btn button-2"><?php echo $label; ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-3 ">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					</div>
				<?php endwhile; ?>

          		
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if( have_rows('customers_journey') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('customers_journey') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $label  = get_sub_field('label');

              ?>

				<div class="col-md-3">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8">
					<h2><?php echo $title; ?></h2>
					<div class="btn-wrap">
					<?php if( $link1 ): ?>
					<a href="<?php echo $link1; ?>" class="btn button-2"><?php echo $label; ?></a>
					<?php endif; ?>
				</div>
				</div>
				<?php endwhile; ?>

			</div>
		</div>
	</section>
	<?php endif; ?>


<?php //get_sidebar(); ?>
<?php get_footer(); ?>



