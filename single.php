<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 */

 get_header('inner'); ?>


<div id="primary" class="container" style="min-height:600px;">
	<main id="main" class="site-main col-md-12" role="main">


		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the single post content template.
			get_template_part( 'template-parts/content', 'single' );

			// If comments are open or we have at least one comment, load up the comment template.
			// if ( comments_open() || get_comments_number() ) {
			// 	comments_template('/comments.php',true);
			// }

			// if ( is_singular( 'attachment' ) ) {
			// 	// Parent post navigation.
			// 	the_post_navigation( array(
			// 		'prev_text' => _x( '<span class="meta-nav">Published in</span><span class="post-title">%title</span>', 'Parent post link', 'twentysixteen' ),
			// 	) );
			// } elseif ( is_singular( 'post' ) ) {
			// 	// Previous/next post navigation.
			// 	the_post_navigation( array(
			// 		'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentysixteen' ) . '</span> ' .
			// 			'<span class="screen-reader-text">' . __( 'Next post:', 'twentysixteen' ) . '</span> ' .
			// 			'<span class="post-title">%title</span>',
			// 		'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentysixteen' ) . '</span> ' .
			// 			'<span class="screen-reader-text">' . __( 'Previous post:', 'twentysixteen' ) . '</span> ' .
			// 			'<span class="post-title">%title</span>',
			// 	) );
			// }

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

	<?php //get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->


<section class="journey">
		<div class="container">
			<div class="col-md-12">
			

            
				
				<div class="col-md-8">
					<h2>Do you have what it takes to unlock
our premium plan?</h2>
<p>Identify the desired actions that meet your business goals,and build customer journeys that specifically support these goals with our gamification bots.</p>
					<div class="btn-wrap">
						<a href="<?php echo site_url(); ?>/pricing" class="btn button-2">Enter the Gami-bot challenge <img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
					</div>
				</div>
				<div class="col-md-3">
					<img src="<?php echo get_template_directory_uri(); ?>/images/gamibot-pacman.png" alt="">
				</div>
				
          	</div>
		</div>
	</section>

<section class="journey">
	<div class="container">
		<div class="col-md-12">
	
          	<div class="col-md-3">
			<img src="<?php echo get_template_directory_uri(); ?>/images/gamiphy-community.png" alt="">
		</div>
		<div class="col-md-8">
			<h2>Transforming your customers journey.</h2>
			<div class="btn-wrap">
				<a href="<?php echo site_url(); ?>/demo" class="btn button-2">Request demo</a> 
			</div>
		</div>
		
		</div>
		
  	</div>

</section>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
