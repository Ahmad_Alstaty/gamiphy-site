<?php
/**
 * Template Name: dy 
 *
 */

 get_header('inner'); ?>


	<!--=================== content area ================================== -->
		<section class="gamibot">
		<div class="container ">
			<div class="hed-left col-md-6">
	        	<img src="<?php echo get_template_directory_uri(); ?>/images/quests.png">
	        	
	        	<?php if( have_rows('header_content') ): ?>


	            <?php while( have_rows('header_content') ): the_row(); 

	              // vars
	              $image = get_sub_field('image');
	              $content = get_sub_field('content');
	              $demo_link  = get_sub_field('demo_link');
	              $demo_link1  = get_sub_field('demo_link1');
	              $link= get_sub_field('link ');
	              $buttontitle = get_sub_field('label');
	              $buttontitle_j = get_sub_field('label_1');

	              ?>

	              <?php echo $content; ?>
	              <div class="btn-wrap">
	                <?php if( $demo_link ): ?>
	                  <a href="<?php echo $demo_link; ?>" class="btn button-1"><?php echo $buttontitle; ?></a>
	                <?php endif; ?>
	                
	                <?php if( $demo_link1 ): ?>
	                  <a href="<?php echo $demo_link1; ?>" class="btn button-2"> <?php echo$buttontitle_j; ?></a>
	                <?php endif; ?>
	               
	              </div>

	        </div>
	        <div class="hed-right col-md-5 pull-right text-right">
	            <div class="row">
	            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
	            </div>
	        </div>

	         <?php endwhile; ?>

	          <?php endif; ?>

	    </div>

	</div>
	</section>

	<section class="gami-repeat">
		<div class="container">
			<div class="col-md-6 left-gimg">
				<img src="<?php echo get_template_directory_uri(); ?>/images/gamibot-games.png">
			</div>
			<div class="col-md-6 right-content">
				
			</div>
		</div>
	</section>




	
	
<section class="journey">
		<div class="container">
			<div class="col-md-12">
			<?php if( have_rows('customers_journey') ): ?>


            <?php while( have_rows('customers_journey') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $link  = get_sub_field('link1');

              ?>

				<div class="col-md-3">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8">
					<h2><?php echo $title; ?></h2>
					<div class="btn-wrap">
					<?php if( $link1 ): ?>
					<a href="<?php echo $link1; ?>" class="btn button-1">Request demo</a>
					<?php endif; ?>
					<?php if( $link ): ?>
					<a href="<?php echo $link; ?>" class="btn button-2">Begin the journey</a>
					<?php endif; ?>
				</div>
				</div>
				<?php endwhile; ?>

          		<?php endif; ?>
			</div>
		</div>
	</section>


		
	<!--=================== content area end ================================== -->

<?php get_footer(); ?>
