<?php
/**
 * Template Name: GAM press  
 *
 */

 get_header('inner'); ?>


<!--=================== content area ================================== -->

<div id="primary" class="container" >
	<main id="main" class="site-main col-md-12" role="main">
		<div class="row blog-head">
			<?php if( have_rows('header_content') ): ?>
            <?php while( have_rows('header_content') ): the_row(); 

              // vars
              $left_image = get_sub_field('left_image');
              $right_image  = get_sub_field('right_image');
              $heading  = get_sub_field('heading');
              $sub_heading  = get_sub_field('sub_heading');

              ?>

			<div class="col-md-2 col-xs-6">
			<img src="<?php echo $left_image['url']; ?>" alt="<?php echo $left_image['alt'] ?>" />
			</div>
			<div class="col-md-8 contentblog col-xs-6">
			<h2><?php echo $heading; ?></h2>
			<p><?php echo $sub_heading; ?></p>
			</div>
			<div class="col-md-2 col-xs-6">
			<img src="<?php echo $right_image['url']; ?>" alt="<?php echo $right_image['alt'] ?>" />
			</div>

			<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</main>
</div>
	
<section class="post-looping press">
	
	<div class="container">
		<div class="row">
		
		<?php
		    
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

			$args = array(
			    'post_type'=>'press', // Your post type name
			    'posts_per_page' => 10,
			    'paged' => $paged,
			);

			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
			    while ( $loop->have_posts() ) : $loop->the_post();

        	$categories = get_the_category(); 
        	
        ?>

            <div class="col-md-4 <?php  echo $categories[0]->name; ?>" >
					<article class="post-art ">
						<a href="<?php the_permalink(); ?>">
							<div class="image-post">
								<div class="gradient <?php  echo $categories[0]->slug; ?>"></div>
							</div>
						</a>
						<div class="art-content">			
							<h3 class="art-title">
							<a href="<?php the_permalink(); ?>" class="art-title"><?php the_title(); ?></a>
							</h3>
							<?php the_content(); ?>
						</div>

					</article>
			</div>

	       <?php endwhile; 
	      
		    $total_pages = $loop->max_num_pages;

		    if ($total_pages > 1){

		        $current_page = max(1, get_query_var('paged'));

		        echo "<div class='pagination col-md-12'>";
		        
		        echo paginate_links(array(
		            'base' => get_pagenum_link(1) . '%_%',
		            'format' => '/page/%#%',
		            'current' => $current_page,
		            'total' => $total_pages,
		            'prev_text'    => __('prev'),
		            'next_text'    => __('next'),
		        ));

		        echo "</div>";

		    }    
		}
		wp_reset_postdata();?>
				
				
		</div>
	</div>
</section>

<?php if( have_rows('unlock_our_premium_plan') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('unlock_our_premium_plan') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content  = get_sub_field('content');
              $label  = get_sub_field('label');
              $link  = get_sub_field('link');

              ?>

					<div class="col-md-8">
						<h2><?php echo $content; ?></h2>
						<div class="btn-wrap">
						<?php if( $link ): ?>
							<a href="<?php echo $link; ?>" class="btn button-2"><?php echo $label; ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-3 ">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					</div>
				<?php endwhile; ?>

          		
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if( have_rows('customers_journey') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('customers_journey') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $label  = get_sub_field('label');

              ?>

				<div class="col-md-3">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8">
					<h2><?php echo $title; ?></h2>
					<div class="btn-wrap">
					<?php if( $link1 ): ?>
					<a href="<?php echo $link1; ?>" class="btn button-2"><?php echo $label; ?></a>
					<?php endif; ?>
				</div>
				</div>
				<?php endwhile; ?>

			</div>
		</div>
	</section>
	<?php endif; ?>


		
	<!--=================== content area end ================================== -->

<?php get_footer(); ?>
