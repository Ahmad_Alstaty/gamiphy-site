<?php
/**
 * Template Name: GAM about  
 *
 */

 get_header(); ?>


	<!--=================== content area ================================== -->

	<div class="container">
		<div class="col-md-6 sub-header gam mx-auto">
			
				<?php $content =  get_field('team_content'); 
					echo $content;
				?>
		</div>		
	</div>	


	<section class="gam-team">
		<div class="container">
			<div class="col-md-12">
				<?php if( have_rows('team') ): ?>


            <?php while( have_rows('team') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $name  = get_sub_field('name');
              $designation  = get_sub_field('designation');
              $short_desc  = get_sub_field('short_desc');
              $user_emoji  = get_sub_field('user_emoji');
              $leaf_left  = get_sub_field('leaf_left');
              $leaf_right  = get_sub_field('leaf_right');
              $speak = get_sub_field('speak');
              
              ?>
				<div class="col-md-3 gam-team-inner">
					<div class="img-team">
						
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>"  class="user-thum" />
						<?php if( $leaf_left ): ?>
						<img src="<?php echo $leaf_left['url']; ?>" alt="<?php echo $leaf_left['alt'] ?>"  class="left-leaf" />
						<?php endif; ?>
						<?php if( $leaf_right ): ?>
						<img src="<?php echo $leaf_right['url']; ?>" alt="<?php echo $leaf_right['alt'] ?>"   class="right-leaf" />
						<?php endif; ?>
						<img src="<?php echo $user_emoji['url']; ?>" alt="<?php echo $user_emoji['alt'] ?>"   class="useremoji" />
							<span class="speak"> <?php echo $speak; ?></span>
						</div>
					
					<div class="over">
						<h5><?php echo $name; ?></h5>
						<p><?php echo $designation; ?></p>	
						<p class="fading"><?php echo $short_desc; ?></p>					
					</div>
					
				</div>
				<?php endwhile; ?>

          		<?php endif; ?>

				
			</div>
		</div>
	</section>
	
	<section class="contact-gamiphy">
		<div class="container">
			<div class="col-md-8 mx-auto">
				
				<?php  $contact_form_text = get_field('contact_form_text' );

					echo $contact_form_text;
			 	?>

				<div class="cta-form ">
					<div class="form-horizontal">
					
					<?php echo do_shortcode('[contact-form-7 id="518" title="Contact form 1"]'); ?>

					</div>
					
				</div>
			</div>
			
			<!-- <div class="col-md-12 text-center ">
				<h3 class="Do-you-have">Do you have what it takes to unlock our premium plan?</h3>
				<p><a href="#" class="learn-more text-center">Enter the Gami-bot challenge  <img src="<?php echo get_template_directory_uri(); ?>/images/arrow-right-copy.png" alt="arrow"></a></p>
			</div> -->
		</div>
	</section>

	<?php if( have_rows('unlock_our_premium_plan') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('unlock_our_premium_plan') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content  = get_sub_field('content');
              $label  = get_sub_field('label');
              $link  = get_sub_field('link');

              ?>

					<div class="col-md-8">
						<h2><?php echo $content; ?></h2>
						<div class="btn-wrap">
						<?php if( $link ): ?>
							<a href="<?php echo $link; ?>" class="btn button-2"><?php echo $label; ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-3 ">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					</div>
				<?php endwhile; ?>

          		
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if( have_rows('customers_journey') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('customers_journey') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $label  = get_sub_field('label');

              ?>

				<div class="col-md-3">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8">
					<h2><?php echo $title; ?></h2>
					<div class="btn-wrap">
					<?php if( $link1 ): ?>
					<a href="<?php echo $link1; ?>" class="btn button-2"><?php echo $label; ?></a>
					<?php endif; ?>
				</div>
				</div>
				<?php endwhile; ?>

			</div>
		</div>
	</section>
	<?php endif; ?>

		
	<!--=================== content area end ================================== -->

<?php get_footer(); ?>
