<?php
/**
 * Template Name: GAM aboutV1
 *
 */

 get_header('inner'); ?>


	<!--=================== content area ================================== -->
	<div class="container">
	<?php if( have_rows('banner') ): ?>
		<?php while( have_rows('banner') ): the_row(); 

              // vars
			  $heading = get_sub_field('heading');
              $image = get_sub_field('image');

              ?>
            <div class="col-xs-12">
				<div class="banner-about" style="background-image:url(<?php echo  $image['url']; ?>)">
					<h1><?php echo $heading ?></h1>
				</div>
			</div>
		<?php endwhile; ?>
		<?php endif; ?>
	</div>


	<?php if( have_rows('brief_intro') ): ?>
	<section class="brief-intro">
		<div class="container">

			<?php while( have_rows('brief_intro') ): the_row(); 

              // vars
			  $content = get_sub_field('content');
              $background = get_sub_field('background');
              $left_image = get_sub_field('left_image');
              $right_image = get_sub_field('right_image');

              ?>
			<div class="col-md-6 col-md-offset-1">
					<?php echo $content; ?>
			</div>

			<div class="mankite" style="background-image:url(<?php echo  $background['url']; ?>)"></div>

			<div class="col-md-12 people-images">
				<div class="col-md-3">
					<img src="<?php echo $left_image['url']; ?>" alt="<?php echo $left_image['alt'] ?>" class="img-responsive" / >
				</div>
				<div class="col-md-6 pull-right">
					<img src="<?php echo $right_image['url']; ?>" alt="<?php echo $right_image['alt'] ?>" class="img-responsive"  />
				</div>
			</div>	
			<?php endwhile; ?>

		</div>	

	</section>

	<?php endif; ?>

	<div class="container">
		<div class="col-md-6 sub-header gam mx-auto">
			
				<?php $content =  get_field('team_content'); 
					echo $content;
				?>
		</div>		
	</div>	

	<section class="team-member">
		<div class="container">
		<?php
		    
			$args = array(
			    'post_type'=>'team', // Your post type name
			    'posts_per_page' => -1,
			);

			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
			    while ( $loop->have_posts() ) : $loop->the_post();
			    $linkedin_url  = get_field('linkedin_url');
        	 $featured_images = $dynamic_featured_image->get_featured_images( $post->ID );
        ?>

			<div class="col-md-3">
				<div class="teamin">
						<?php  $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');  ?>
						<div class="image-team" style="background-image:url(<?php echo  $featured_img_url ; ?>)"></div>
						<?php 
						if ( $featured_images ):
        				?>
				            <?php foreach( $featured_images as $images ): ?>
				               <div class="image-team2" style="background-image:url(<?php echo $images['full'] ?>)"></div>
				            <?php endforeach; ?>
				        <?php
				        endif;
				         ?>
						

					<h3><a href="<?php echo $linkedin_url; ?>"> <?php the_title(); ?> </a></h3>
					<p><?php the_excerpt(); ?></p>
				</div>
			</div>

			 <?php endwhile; }
		wp_reset_postdata();?>

		</div>
	</section>

	<section class="talkabout">
		<div class="container">
			<div class="col-md-12">
			
			<?php if( have_rows('talk_about') ): ?>

            <?php while( have_rows('talk_about') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content  = get_sub_field('content');

              ?>

				<div class="col-md-5">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" / class="img-responsive">
				</div>
				<div class="col-md-6 pull-right">
					<?php echo $content; ?>					
				</div>
				</div>
				<?php endwhile; ?>

			</div>
		</div>
	</section>
	<?php endif; ?>


	<section class="contact-gamiphy">
		<div class="container">
			<div class="col-md-8 mx-auto">
				
				<?php  $contact_form_text = get_field('contact_form_text' );

					echo $contact_form_text;
			 	?>

				<div class="cta-form ">
					<div class="form-horizontal">
					
					<?php echo do_shortcode('[contact-form-7 id="518" title="Contact form 1"]'); ?>

					</div>
					
				</div>
			</div>
		</div>
	</section>


	<?php if( have_rows('unlock_our_premium_plan') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('unlock_our_premium_plan') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $content  = get_sub_field('content');
              $label  = get_sub_field('label');
              $link  = get_sub_field('link');

              ?>

					<div class="col-md-8">
						<h2><?php echo $content; ?></h2>
						<div class="btn-wrap">
						<?php if( $link ): ?>
							<a href="<?php echo $link; ?>" class="btn button-2"><?php echo $label; ?>
							<img src="<?php echo get_template_directory_uri(); ?>/images/ic-go.svg"></a>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-3 ">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					</div>
				<?php endwhile; ?>

          		
			</div>
		</div>
	</section>
	<?php endif; ?>

	<?php if( have_rows('customers_journey') ): ?>
	<section class="journey">
		<div class="container">
			<div class="col-md-12">
			


            <?php while( have_rows('customers_journey') ): the_row(); 

              // vars
              $image = get_sub_field('image');
              $title  = get_sub_field('title');
              $link1  = get_sub_field('link1');
              $label  = get_sub_field('label');

              ?>

				<div class="col-md-3">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
				</div>
				<div class="col-md-8">
					<h2><?php echo $title; ?></h2>
					<div class="btn-wrap">
					<?php if( $link1 ): ?>
					<a href="<?php echo $link1; ?>" class="btn button-2"><?php echo $label; ?></a>
					<?php endif; ?>
				</div>
				</div>
				<?php endwhile; ?>

			</div>
		</div>
	</section>
	<?php endif; ?>

		
	<!--=================== content area end ================================== -->

<?php get_footer(); ?>
