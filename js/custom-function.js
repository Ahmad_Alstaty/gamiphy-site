
// box height

$(document).ready(function(){

    var highestBox = 0;
        $('.plansgami .plans-gami-inner').each(function(){  
                if($(this).height() > highestBox){  
                highestBox = $(this).height();  
        }
    });    
    $('.plansgami .plans-gami-inner').height(highestBox);

});


//  pricing filter


  $(function() {
      $("#slider-range").slider({
          range: "min",
          animate: true,
          value:0,
          range: true,
          min: 5000,
          max: 50000,
          step: 50,
          slide: function(event, ui) {
            update(1,ui.value); //changed
          }
      });


      //Added, set initial value.
      $("#amount").val();
      //$("#amount-label").text() 



      update();
  });

  //changed. now with parameter
  function update(slider,val) {
    //changed. Now, directly take value from ui.value. if not set (initial, will use current value.)
    var $amount = slider == 1?val:$("#amount").val();
    var $dollar = 100;
    var $user = '10,000';

   
    if( $amount > 10000)
    {
        var aboveTheRangeUsers =  $amount - 10000;
        var increament = aboveTheRangeUsers/1000;
        var dollarIncreament = increament*20;
        var updatedDollarAmount = $dollar + dollarIncreament;
        $('#dollar').val(updatedDollarAmount);  
         $( "#user-label" ).text($amount)
    }
    else 
    {
        $('#dollar').val(100);
        $( "#user-label" ).text($user);
    }
     $( "#amount" ).val($amount);
     

	}

			
//toltip

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

